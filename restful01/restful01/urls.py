from django.conf.urls import url, include
from django.contrib import admin

# Django 2.0+ breaking change
app_name = 'main_app'

'''
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^', include('toys.urls')),
    url(r'^', include('drones.urls')),
    url(r'^api-auth/', include('rest_framework.urls'))
]
'''

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    #url(r'^', include('toys.urls')),
    url(r'^v1/', include('drones.urls', namespace='v1')),
    url(r'^v1/api-auth/', include('rest_framework.urls', namespace='rest_framework_v1')),
    url(r'^v2/', include('drones.v2.urls', namespace='v2')),
    url(r'^v2/api-auth/', include('rest_framework.urls', namespace='rest_framework_v2')),
]