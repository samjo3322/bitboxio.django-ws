# README #



### What is this repository for? ###

* Quick summary - This project is to further develop python best practices and learn implimentation of django rest framework (DRW). It's based on the book 'Djano RESTful Web Services' by Gaston C. Hillar. The book references older versions of Django and DRF but is mostly 99% accurate.

### What did I learn... ###

* Summary - DRF is very prescriptive. That's not a bad thing if your care about the little things an need a enterprise solution. The framework is mature with lots of good tooling and of course PYTHON ROCK!
* Use python virtual environment (just make it a habbit)
* Understand Django Models and the ORM (plan your api seperate from the gui and even db somewhat)
* The manage.py is REALLY NICE!!!
* API Features (Browsable) - Pagination, Backend Filters (sort, order, search), Authentication & Permissions (Token), Throttling, Versioning (Namespace)
* Creating Users in Djano
* Testing and Coverage

### Django DRF ###
* Activate Python VENV
$ source ~/bitboxio.django-ws/bin/activate

* Run Dev Server:
$ cd .../restful01/python manage.py runserver 0.0.0.0:8000
$ ctl + C (stop)

### Docker Containter ###
* https://docs.docker.com/compose/django/ 
* Containers (see files): Dockerfile and docker-compose.yml (mydb + web + nginx)
* Packages - run pip freeze and update requiremnts.txt
* run: $ docker-compose up --build
* stop: ctl + C

### TODO ###
* Enhance docker-compose with secrets
* Put docker container behind nginx with ssl before point GUIs at it
* Add a gui with react-native (ios / android)

### Who do I talk to? ###
* Sam Crouch - samc3322@gmail.com